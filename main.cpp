
///importing libraries
#include <iostream>
#include <fstream>
using namespace std;
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
class agg{
public:
	///function to calculate aggregate of the students 
	double cal(double a, double b, double c, double d, double e, double f, double g, double h, double i){
		double quiz = double((a + b + c) / 30.0) * 10.0;
		double assignment = ((d + e + f) / 30.0) * 10.0;
		double OHT = ((g + h) / 100.0) * 40.0;
		double ESE = (i / 100.0) * 40.0;
		
		return quiz + assignment + OHT + ESE;
	}
	///function to claculate grade based the aggregate
	string grade(double x,double y){	

		////grading criteria////

		if (x >= y+15.0){
			return "A";
		}
		if (x >=y+10.0 &&x<=y+15){										
			return"B+";
		}
		if (x >= y-5.0 && x <y+5.0){
			return"B";
		}
		if (x >= y-10.0 && x < y-5.0){
		return"C+";
		}
		if (x >= y-15.0 && x < y-10.0){
		return"C";
		}
		if (x >= y-20.0 && x < y-15.0){
		return"D+";
		}
		if (x >= y-25.0 && x< 20.0){
			return"D";
		}
		if (x < y-25.0){
			return"F";
		}


	}
};
///main method
int main()
{
	agg obj;                           /// creating object of class
	ofstream myfile;                  /// creating object of ofstream class
	ifstream ip("E:\\labs\\assignment\\Sample-Gradebook-copy.csv");   /// giving address of file from which data has to be read to the ifsream object

	string cms;
	//variables for storing data
	string line;
	string fname, lname;
	
	int a = 0, f = 0,b=0;
	double lines = 0.0; ///variable to store total number of lines in csv file
	while (getline(ip, line)){
		++lines;
	}
	ip.clear();
	ip.seekg(0, ios::beg);  ///moving cursor to start of file




	/// average claculation////
	double average = 0.0;
	int g = 0;
	while (!ip.eof()){
		std::getline(ip, line);
		
		if (g > 1){

			double Q1, Q2, Q3, OHT1, OHT2, ESE, A1, A2, A3;
			///loop till the last line of file /end of file
			///storing the line which is being read in line variable

			std::replace(line.begin(), line.end(), ',', ' '); ///replacing commas with empty spaces
			stringstream ss(line);                            ///making streamstring of line
			ss >> cms;
			ss >> fname;
			ss >> lname;         /// ASSIGNING  VALUES TO THE VARIABELS
			ss >> Q1;
			ss >> A1;
			ss >> A2;
			ss >> A3;
			ss >> Q2;
			ss >> Q3;
			ss >> OHT1;
			ss >> OHT2;
			ss >> ESE;


			double x = obj.cal(Q1, Q2, Q3, A1, A2, A3, OHT1, OHT2, ESE);/// passing variables to the cal function to 
			
			average += x;
		}
		g++;
	}
	double avgagg = average / (lines - 2);
	cout << "average is" << avgagg << endl;
	ip.clear();
	ip.seekg(0, ios::beg);
	/// average claculation
			
			int c = 0;
			while (!ip.eof()){
				std::getline(ip, line);
				
				if (c > 1){

					double Q1, Q2, Q3, OHT1, OHT2, ESE, A1, A2, A3;
					///loop till the last line of file /end of file
					///storing the line which is being read in line variable

					std::replace(line.begin(), line.end(), ',', ' '); ///replacing commas with empty spaces
					stringstream ss(line);                            ///making streamstring of line
					ss >> cms;
					ss >> fname;
					ss >> lname;         /// ASSIGNING  VALUES TO THE VARIABELS
					ss >> Q1;
					ss >> A1;
					ss >> A2;
					ss >> A3;
					ss >> Q2;
					ss >> Q3;
					ss >> OHT1;
					ss >> OHT2;
					ss >> ESE;


					double x = obj.cal(Q1, Q2, Q3, A1, A2, A3, OHT1, OHT2, ESE);/// passing variables to the cal function to 
					cout << x << endl;
				
			
			///calculate aggregate
			


			string grade = obj.grade(x,avgagg); /// passing aggregate to grade function to get the grade
			cout << grade << endl;

			if (grade == "A"){     ///condition for nubmer of A grades to be less than 10
				if (a >= 10){   ///no more than 10 A grades
					myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-Bplus.csv", ios::out | ios::app);
					myfile << fname + " " + " " + lname + ",";
					myfile << cms;
					myfile << endl;
					myfile.close();
				}
				else{
					myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-A.csv", ios::out | ios::app); ///opening gradeA file
					myfile << fname + " " + " " + lname + ","; ///parsing name
					myfile << cms;								///parsing cms id
					myfile << endl;								///moving cursor to next line
					myfile.close();								///closing the file
					a+=1;
				}

			}
			if (grade == "B+"){


				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-Bplus.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}
			if (grade == "B"){
				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-B.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}
			if (grade == "C+"){
				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-Cplus.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}
			if (grade == "C"){
				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-C.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}

			if (grade == "D+"){


				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-Dplus.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}
			if (grade == "D"){



				myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-D.csv", ios::out | ios::app);
				myfile << fname + " " + " " + lname + ",";
				myfile << cms;
				myfile << endl;
				myfile.close();


			}



			if (grade == "F"){
				if (f >= 10){        ///dont give more than 10 F grades
					myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-D.csv", ios::out | ios::app);
					myfile << fname + " " + " " + lname + ",";
					myfile << cms;
					myfile << endl;
					myfile.close();
				}
				else{
					myfile.open("E:\\labs\\assignment\\BESE-8AB-DSA-F.csv", ios::out | ios::app);
					myfile << fname + " " + " " + lname + ",";
					myfile << cms;
					myfile << endl;
					myfile.close();
					f++;
					
				}


			}


		}
		c++;
	}
	ip.close();///closing the file
}
